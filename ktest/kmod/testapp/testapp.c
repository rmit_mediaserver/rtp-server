#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>


int main(void) {
    int fd;
    int *mem = NULL;
    int i;
    
    fd = open("/dev/jpm_test0", O_RDWR);

    mem = mmap(NULL, 64000, PROT_READ | PROT_WRITE, 
                                                MAP_SHARED, fd, 0);

    if (mem == MAP_FAILED) {
        perror("mmap failed\n");
    }

    for (i = 1; i < 5; i++) {
        printf("Trying to read %d times\n", i);
        if (read(fd, NULL, 0) == -1) {
            printf("received error msg\n");
            return -1;
        }
    }

    printf("done all\n");
    close(fd);
    return 0;
}
