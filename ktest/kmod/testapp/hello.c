#include "linux/linkage.h"
#include <linux.kernel.h>

asmlinkage int sys_hello() {
    printk (KERN_ALERT "Hello\n");
    return 1;
}
