#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/mman.h>
#include <linux/slab.h>
#include <linux/semaphore.h>
#include <linux/jiffies.h>
#include <linux/kernel.h>

#include <linux/netdevice.h>
#include <linux/ip.h>
#include <linux/in.h>
#include <linux/delay.h>

#include <linux/workqueue.h>

#define JPM_ELEMENT_SIZE    10
#define JPM_BUFFER_SIZE     1616

#define JPM_SIZE            sizeof(struct jpm_shm)
#define JPM_TIMEOUT         3000 /*milliseconds*/

#define JPM_ADDR_OTHER      0xC0A801F4 /* 192.168.1.244 */
#define JPM_ADDR_LOOP       0x7f000001 /* 127.0.0.1 */
#define JPM_PORT_OTHER      9930
#define JPM_PORT_ME         9930

#include "jpm_mod.h"

struct contain_packet {
    uint32_t actualSize;
    uint32_t deltaTime;
    uint8_t packet[JPM_BUFFER_SIZE];
};

struct jpm_shm {
        struct contain_packet mem[JPM_ELEMENT_SIZE];
};

struct wq_wrapper {
    struct work_struct worker;
};

struct jpm_clients {
    struct sockaddr_in list[100];
    uint32_t numClients;
};

static struct workqueue_struct *wq;
static struct jpm_clients *clients;
static struct jpm_shm *pages_mem;

static struct socket *sock;

static struct semaphore *emptyCount;
static struct semaphore *fillCount;

static unsigned long pages_area = 0;

int ksocket_send(   struct socket *sock,
                    struct sockaddr_in *addr,
                    uint8_t *buf,
                    int len);

void send_all(struct work_struct*);

struct wq_wrapper wq_data;

static DECLARE_COMPLETION(threadcomplete);

static ssize_t jpm_write (  struct file *fd,
                            const char __user* ubuffer,
                            size_t length,
                            loff_t *offset) {
    char buffer[22];
    char addr_str[16];
    char port_str[6];
    int buffSize = 22;
    unsigned long port;
    unsigned long addr;
    int i;
    int j;

    memset(buffer, 0, 22);
    if (length < 4) {
        printk("jpm_write - length is less than four\n");
        return -1;
    }
    buffSize = buffSize > length ? length : buffSize;
    if (copy_from_user(buffer, ubuffer, buffSize)) {
        return -EFAULT;
    }
    i = 0;
    j = 0;
    memset(addr_str, 0, 16);
    memset(port_str, 0, 6);
    
    while (buffer[i] != ':') {
        if (i == 15) {
            printk("cannot find addr\n");
            return -1;
        }
        addr_str[i] = buffer[i];
        ++i;
    }
    i++; /* skip the delimiter */
    while (buffer[i] != '\0') {
        port_str[j++] = buffer[i++];
    }
    if (kstrtol(port_str, 10, &port)) {
        printk("kstrtol errored\n");
        return -1;
    }
    if (kstrtoul(addr_str, 16, &addr)) {
        printk("kstrtol errored on addr |%s|\n", addr_str);
        return -1;
    }

    memset(clients->list + clients->numClients, 0, sizeof(struct sockaddr_in));
    clients->list[clients->numClients].sin_family = AF_INET;
    clients->list[clients->numClients].sin_port = htons(port);
    clients->list[clients->numClients].sin_addr.s_addr = htonl(addr);
    (clients->numClients)++;

    return 0;
}

static long jpm_ioctl_sem(  struct file *fd,
                            unsigned int cmd, 
                            unsigned long data) {
    switch (cmd) {
        case IOCTL_DOWN_SEM:
            if (down_timeout(   emptyCount, 
                                msecs_to_jiffies(1000)) == -ETIME) {
                printk("down sem ioctl timed out \n");
                return -1;
            }
            break;
        case IOCTL_UP_SEM:
            up(fillCount);
            break;
        default:
            printk("unknown ioctl\n");
            return -1;
    }
    return 0;
}

static int jpm_mmap(struct file *filp, struct vm_area_struct *vma) {
    int rc = 0;
    rc = remap_pfn_range(   vma, 
                            vma->vm_start, 
                            virt_to_phys((void*)pages_area) >> PAGE_SHIFT,
                            vma->vm_end - vma->vm_start,
                            vma->vm_page_prot);
    if (rc < 0) {
        return - EAGAIN;
    }
    flush_workqueue(wq);
    queue_work(wq, &wq_data.worker);
    return 0;
}

static const struct file_operations test_fops = {
    .owner = THIS_MODULE,
    .unlocked_ioctl = jpm_ioctl_sem,
    .mmap = jpm_mmap,
    .write = jpm_write
};

static int test_init(void) {
    int i;

    i = register_chrdev(JPM_DEVICEMAJOR, JPM_DEVICENAME, &test_fops);

    if (i != 0) {
        printk("regist_chrdev failed\n");
        return -EIO;
    }

    pages_area = __get_free_pages(  GFP_KERNEL | GFP_DMA, 
                                    get_order(JPM_SIZE));
    
    if (!pages_area) {
        printk("__get_free_pages failed\n");
        unregister_chrdev(JPM_DEVICEMAJOR, JPM_DEVICENAME);
        return -1;
    }

    if ((fillCount = kmalloc(sizeof(struct semaphore), GFP_KERNEL)) == NULL) {
        printk("kmalloc fillCount failed\n");
        unregister_chrdev(JPM_DEVICEMAJOR, JPM_DEVICENAME);
        free_pages(pages_area, get_order(JPM_SIZE));
        return -1;
    }

    if ((emptyCount = kmalloc(sizeof(struct semaphore),GFP_KERNEL)) == NULL) {
        printk("kmalloc emptyCount failed\n");
        unregister_chrdev(JPM_DEVICEMAJOR, JPM_DEVICENAME);
        free_pages(pages_area, get_order(JPM_SIZE));
        kfree(fillCount);
        return -1;
    }

    pages_mem = (struct jpm_shm*)pages_area;
    sema_init(fillCount , 0);
    sema_init(emptyCount, JPM_ELEMENT_SIZE);
    clients = kmalloc(sizeof(struct jpm_clients), GFP_KERNEL);
    memset(clients, 0, sizeof(struct jpm_clients));

    INIT_WORK(&wq_data.worker, send_all);
    wq = create_singlethread_workqueue("workqueue");
    if (!wq) {
        printk("workqueue error\n");
        return -ENOMEM;
    }
    
    printk("JPM_KERNEL INIT\n");
    return 0;
}

static void test_exit(void) {
    kfree(emptyCount);
    kfree(fillCount);
    free_pages(pages_area, get_order(JPM_SIZE));
    unregister_chrdev(JPM_DEVICEMAJOR, JPM_DEVICENAME);
    if (wq) {
        flush_workqueue(wq);
        destroy_workqueue(wq);
    }

    printk("JPM_KERNEL EXIT\n");
}

void send_all(struct work_struct *data) {
    uint32_t front;
    uint32_t deltaTime;
    uint32_t remainerTime;
    int i;
    int done = 0;

    if (sock_create(AF_INET, SOCK_DGRAM, IPPROTO_UDP, &sock) < 0) {
        printk("sock_create faild\n");
        return;
    }

    front = 0;
    deltaTime = 0;
    remainerTime = 0;

    while (!done) {
            if (    down_timeout(fillCount,
                    msecs_to_jiffies(1000)) == -ETIME) {
                /* timeout */
                done = 1;
                break;
            }
        for (i = 0; i < clients->numClients; ++i) {
            if (ksocket_send(   sock, 
                                &(clients->list[i]), 
                                pages_mem->mem
                                        [front % JPM_ELEMENT_SIZE].packet, 
                                pages_mem->mem
                                        [front % JPM_ELEMENT_SIZE].actualSize)
                                            < 0) {
                printk("ksocket_send error?\n");
            }
        }
        deltaTime = pages_mem->mem[front % JPM_ELEMENT_SIZE].deltaTime;
        front++;
        up(emptyCount);
        if (done) {
            continue;
        }
        msleep_interruptible(((deltaTime + remainerTime) / 2000000u));
        remainerTime = (deltaTime + remainerTime) % 2000000u;
    }
    /* reset the values for next time */
    sema_init(fillCount , 0);
    sema_init(emptyCount, JPM_ELEMENT_SIZE);
    memset(clients, 0, sizeof(struct jpm_clients));

    printk("Finished sending\n");
}

int ksocket_send(   struct socket *sock,
                    struct sockaddr_in *si_o,
                    uint8_t *buff,
                    int len) {
    struct msghdr msgh;
    struct iovec iov;
    mm_segment_t oldfs;
    int size = 0;

    iov.iov_base = buff;
    iov.iov_len = len;

    msgh.msg_flags = MSG_DONTWAIT;
    msgh.msg_name = si_o;
    msgh.msg_namelen = sizeof(struct sockaddr_in);
    msgh.msg_control = NULL;
    msgh.msg_controllen = 0;
    msgh.msg_iov = &iov;
    msgh.msg_iovlen = 1;
    msgh.msg_control = NULL;

    oldfs = get_fs();
    set_fs(KERNEL_DS);
    size = sock_sendmsg(sock, &msgh, len);
    set_fs(oldfs);

    return size;
}

module_init(test_init);
module_exit(test_exit);

MODULE_AUTHOR("Paul Hoang");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Test");
