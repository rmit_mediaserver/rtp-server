#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#define BUFLEN 512
#define NPACK 10
#define PORT 9930
#define SRV_IP "127.0.0.1"

int main(int argc, char* argv[]) {
    struct sockaddr_in si_other;
    int s, i, slen;
    char buf[BUFLEN];
    
    slen = sizeof(si_other);

    if (argc != 2) {    
        printf("One input required\n");
        exit(-1);
    }

    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        perror("socket");
        exit(-1);
    }

    memset((char*)&si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);
    if (inet_aton(SRV_IP, &si_other.sin_addr) == 0) {
        fprintf(stderr, "inet_aton() failed\n");
        exit(-1);
    }

    sprintf(buf, "%s\n", argv[1]);
    if (sendto(s, buf, BUFLEN, 0, 
            (struct sockaddr*)&si_other, slen) == -1) {
        perror("sendto()");
        exit(-1);
    }

    close(s);
    return 0;
}

