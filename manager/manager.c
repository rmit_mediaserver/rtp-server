#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>

#define _BSD_SOURCE
#define BASE_PORT       9930
#define BUFFLEN         100
#define MSGLEN          128
#define MAX_SESSIONS    1024
#define LOOPBACK_ADDR   "127.0.0.1"

#define PROGRAM_PATH    "../rtp/send"

typedef struct sessions_list s_list_t;
int search_session(s_list_t*, char*);

struct stream_sessions {
    int fd;
    char filename[BUFFLEN];
    char pipepath[BUFFLEN];
};

struct sessions_list {
    struct stream_sessions list[MAX_SESSIONS];
};

void start_stream(char *filename, char *srcport, char *dstip, char *dstport) {
    int pid;

    signal(SIGCHLD, SIG_IGN);

    pid = fork();

    if (pid < 0) {
        printf("Fork failed\n");
        exit(-1);
    } else if (pid == 0) {
        execl(PROGRAM_PATH, "/send" ,filename, srcport, dstip, dstport, NULL);
        printf("exec failed\n");
        exit(-1);
    }
}

int make_session(s_list_t *all_sessions, char *filename) {
    char npipe[BUFFLEN];
    int fd;
    FILE *fp;

    if ((fp = fopen(filename, "r")) == NULL) {
        perror("make_session - fopen failed\n");
        printf("file is |%s|\n", filename);
        return -1;
    } else {
        fclose(fp);
    }
    strcpy(npipe, "/tmp/");
    strcat(npipe, filename);
    if (mkfifo(npipe, 0777) < 0) { /* 0777 otherwise open will fail */
        perror("make_session - mkfifo failed\n");
        exit(-1);
    }
    if ((fd = open(npipe, O_NONBLOCK)) < 0) {
        perror("make_session - open failed\n");
        exit(-1);
    }
    close(fd);
    if (fd > MAX_SESSIONS) {
        printf("fd is greater than max_sessions\n");
        return -1;
    }
    memset(all_sessions->list[fd].filename, 0, BUFFLEN);
    all_sessions->list[fd].fd = fd;
    strcpy(all_sessions->list[fd].filename, filename);
    strcpy(all_sessions->list[fd].pipepath, npipe);
    
    return fd;
}

/* the write pipe on the manager side has not been opened.
 * This opens it.
 */
void start_session(s_list_t *all_sessions, char *filename) {
    int index;
    int fd;

    if ((index = search_session(all_sessions, filename)) < 0) {
        printf("start_session - search index failed, ");
        printf("can't find newly created session?\n");
        exit(-1);
    }
    if ((fd = open(all_sessions->list[index].pipepath, O_WRONLY)) < 0) {
        perror("start_session - cannot open pipe\n");
        printf("index is %d\n", index);
        printf("pipe path is |%s|\n", all_sessions->list[index].pipepath);
        printf("filename is |%s|\n", all_sessions->list[index].filename);
        exit(-1);
    }
    all_sessions->list[index].fd = fd;
    return;
}

/* there must be a better way. Oh well... */
int search_session(s_list_t *all_sessions, char* filename) {
    struct stream_sessions session;
    int i;
    int returnCode = -1;

    for (i = 0; i < MAX_SESSIONS; i++) {
        session = all_sessions->list[i];
        if (strcmp(session.filename, filename) == 0) {
            returnCode = i;
            break;
        }
    }
    return returnCode;
}

void *listen_streamers(void *thread_args) {
    s_list_t *all_sessions;
    char npipe[BUFFLEN];
    char msg[BUFFLEN];
    char pipeTemp[BUFFLEN];
    char fileTemp[BUFFLEN];
    int fdTemp;
    int fd;
    int index;

    all_sessions = (s_list_t*)thread_args;
    strcpy(npipe, "/tmp/manager");
    if (mkfifo(npipe, 0777) < 0) { /* 777 otherwise open will fail */
        perror("listen_streamers - mkfifo failed\n");
        exit(-1);
    }
    if ((fd = open(npipe, O_RDONLY)) < 0) {
        perror("listen_streamers - open failed\n");
        exit(-1);
    }


    while (1) {
        memset(msg, 0, BUFFLEN);
        if (read(fd, msg, BUFFLEN) < 0) {
            perror("listen_streamers - read failed\n");
            exit(-1);
        }
        if (strlen(msg) == 0) {
            if (close(fd) < 0) {
                perror("listen streamers - close failed\n");
                exit(-1);
            }
            if ((fd = open(npipe, O_RDONLY)) < 0) {
                perror("listen streamers - open failed\n");
                exit(-1);
            }
            printf("it was null\n");
            continue;
        }
        if (strcmp(msg, "shutdown") == 0) {
            /* got shutdown signal from THIS PROCESS */
            printf("shutting down listen streamers\n");
            break;
        }
        if ((index = search_session(all_sessions, msg)) < 0) {
            printf("listen streamers - cannot find search session\n");
            exit(-1);
        }
        /* unlink first before closing to prevent any concurrency
         * issues
         */
        strcpy(pipeTemp, all_sessions->list[index].pipepath);
        strcpy(fileTemp, all_sessions->list[index].filename);
        fdTemp = all_sessions->list[index].fd;

        all_sessions->list[index].fd = -1;
        memset(all_sessions->list[index].filename, 0, BUFFLEN);
        memset(all_sessions->list[index].pipepath, 0, BUFFLEN);

        if (unlink(pipeTemp) < 0) {
            perror("unlink failed\n");
        }
        if (close(fdTemp) < 0) {
            perror("close failed\n");
        }
        printf("manager - Ending session |%s|\n", fileTemp);
    }
    unlink("/tmp/manager");
    close(fd);
    return NULL;
}

void listen_udp(s_list_t *all_sessions) {
    struct sockaddr_in si_me, si_other;
    int s, slen;
    char *addr_other;
    char filename[BUFFLEN];
    char *dstport;
    char srcport[BUFFLEN];
    char buff[BUFFLEN];
    char msg[BUFFLEN];
    int fd;
    int index;

    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        perror("Socket failed\n");
        exit(-1);
    }

    memset((char*)&si_me, 0, sizeof(struct sockaddr_in));
    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(BASE_PORT);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);
    slen = sizeof(struct sockaddr_in);

    if (bind(s, (struct sockaddr*)&si_me, sizeof(struct sockaddr_in)) == -1) {
        perror("Bind failed\n");
        exit(-1);
    }

    while (1) {
        memset(buff, 0, BUFFLEN);
        memset(msg, 0, BUFFLEN);
        memset(filename, 0, BUFFLEN);
        memset(srcport, 0, BUFFLEN);
        if (recvfrom(s, buff, BUFFLEN, 0, 
              (struct sockaddr*)&si_other, (socklen_t*)&slen) == -1) {
            perror("recvfrom error\n");
            exit(-1);
        }
        if (strcmp(buff, "shutdown") == 0 ||
            strcmp(buff, "shutdown\n") == 0) {
            /* got shutdown request */
            break;
        }
        addr_other = inet_ntoa(si_other.sin_addr);
        dstport = memchr(buff, ':', BUFFLEN);
        strncpy(filename, buff, dstport - buff);
        dstport++; /* skip the delimiter */
        if (strtol(dstport, NULL, 10) == 0) {
            printf("Received dstport from UDP is not a number. ");
            printf("Continuing Operation\n");
            continue;
        }
        strcpy(msg, addr_other);
        strcat(msg, ":");
        strcat(msg, dstport);
        
        if ((index = search_session(all_sessions, filename)) < 0) {
            /* not found, make one */    
            if ((fd = make_session(all_sessions, filename)) < 0) {
                /* file cannot be found */
                printf("Cannot make a session ");
                printf("Continuing operations\n");
                continue;
            } else {
                /* all is good, start the stream */
                sprintf(srcport, "%d", BASE_PORT + fd + 1);
                start_stream(filename, srcport, addr_other, dstport);
                start_session(all_sessions, filename);
                printf("started session |%s|\n", filename);
            }
        } else {
            if (all_sessions->list[index].fd < 0) {
                printf("list udp - index returned is -1\n");
                exit(-1);
            }
            if (write(all_sessions->list[index].fd, msg, sizeof(msg)) < 0) {
                printf("manager - write failed\n");
                exit(-1);
            }
        }
    }
    return;
}

int main(int argc, char **argv) {
    s_list_t *all_sessions;
    pthread_t thread_listen_streamers;
    int rc_thread;
    char npipe[BUFFLEN];
    char shutdownMsg[BUFFLEN];
    int fd;
    int i;

    memset(npipe, 0, BUFFLEN);
    strcpy(npipe, "/tmp/manager");

    if ((fd = open("/tmp/manager", O_NONBLOCK)) >= 0) {
        perror("/tmp/manager already exists\n");
        close(fd);
        printf("Unlinking /tmp/manager\n");
        unlink("/tmp/manager");
        printf("Please try again\n");
        exit(-1);
    } 

    all_sessions = calloc(1, sizeof(s_list_t));

    rc_thread = pthread_create( &thread_listen_streamers, 
                                NULL, 
                                listen_streamers,
                                all_sessions);
    
    listen_udp(all_sessions);
    /*cleanup*/
    memset(shutdownMsg, 0, BUFFLEN);
    strcpy(shutdownMsg, "shutdown");
    if ((fd = open(npipe, O_NONBLOCK)) < 0) {
        printf("cleanup open failed\n");
        exit(-1);
    } else {
        close(fd);
        if ((fd = open(npipe, O_WRONLY)) < 0) {
            printf("cleanup second open failed\n");
            exit(-1);
        } else {
            write(fd, shutdownMsg, sizeof(shutdownMsg));
            close(fd);
        }
    }
    
    if (pthread_join(thread_listen_streamers, NULL) < 0) {
        printf("pthread joni error\n");
        exit(-1);
    }

    for (i = 0; i < MAX_SESSIONS; ++i) {
        if (all_sessions->list[i].fd > 0) {
            unlink(all_sessions->list[i].filename);
            close(all_sessions->list[i].fd);
        }
    }

    free(all_sessions);    

    printf("manager end\n");

    return 0;   
}
