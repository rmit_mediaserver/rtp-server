#ifndef _RTP_FRAME_H_

#include "rtputil.h"

typedef struct rtp_hdr rtp_hdr_t;
typedef struct rtp_info rtp_info_t;

struct rtp_hdr {
    uint8_t data[12];
};

struct rtp_hdr_b {
    unsigned int p:1;       /* padding */
    unsigned int x:1;       /* extension */
    unsigned int version:2; /* Protocol Version */
    unsigned int cc:4;      /* csrc count */
    unsigned int m:1;       /* marker */
    unsigned int pt:7;      /* payload type */
    uint16_t seq;           /* sequence number */
    uint32_t ts;            /* timestamp */
    uint32_t ssrc;          /* syncron source */
};

struct rtp_info {
    uint32_t sample;
    uint32_t framerate;
    uint32_t paddingSize;
    uint32_t mpegPayloadSize;
    uint32_t timestampStart;
    uint32_t numFrames;     /* counts the num of pic hdrs starting from 0 */
    uint32_t prevFrames;
    bool changedFrame;
    bool seenEnd;
};

void populateRTPHeader(rtp_hdr_t*, rtp_hdr_t*, rtp_info_t*);
rtp_hdr_t* createRTPHeader(rtp_hdr_t*, rtp_info_t*);
#define _RTP_FRAME_H_
#endif


