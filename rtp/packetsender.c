#include "rtputil.h"
#include "rtpfile.h"
#include "rtpframe.h"
#include "rtpmpegframe.h"
#include "rtpfileparser.h"
#include "packetsender.h"

#include <arpa/inet.h>
#include <pthread.h>
#include <poll.h>
#include <sys/time.h>

#define PORT_ME     9930
#define MAX_MSG     128
#define MAX_CLIENTS 100

struct rp_arg {
    struct sockaddr_in si_all[MAX_CLIENTS];
    int *top_send;
    char filename[MAX_MSG];
};

int produce_packets(    char *filePath,
                        packet_t *buff, 
                        sem_t *fillCount,
                        sem_t *emptyCount,
                        uint32_t *last) {
    FILE *fp;
    mem_t *cMem;   
    hdrs_t *hdrs;
    rtp_hdr_t *prevHdr;
    rtp_hdr_t *currHdr;
    rtp_info_t *info;
    mpeg_packet_t *mpegPacket;
    uint32_t u_sec;

    fp = openMPEG(filePath);
    hdrs = safe_malloc(sizeof(hdrs_t));
    cMem = safe_malloc(sizeof(mem_t));
    info = safe_malloc(sizeof(rtp_info_t));
    prevHdr = NULL;

    loadFiletoMem(fp, 0, SEEK_SET, cMem);

    while(true) {
        mpegPacket = createMPEGPacket(fp, cMem, hdrs, info);

        if (info->sample == 3003) { 
            u_sec = 33366700;
        } else {
            printf("this samplerate isn't supoprted\n");
            exit(-1);
        }

        currHdr = createRTPHeader(prevHdr, info);

        sem_wait(emptyCount);
        buff[(*last % SHM_NUM_ELEMENTS)].actualSize =
                    info->mpegPayloadSize + sizeof(rtp_hdr_t) + 
                                                sizeof(mpeg_vid_hdr_t);
        buff[(*last % SHM_NUM_ELEMENTS)].deltaTime = 
                    info->changedFrame?u_sec:0;
        memcpy(buff[(*last % SHM_NUM_ELEMENTS)].packet, 
                                            currHdr, sizeof(rtp_hdr_t));
        memcpy(buff[(*last % SHM_NUM_ELEMENTS)].packet + sizeof(rtp_hdr_t), 
                                            mpegPacket, sizeof(mpeg_packet_t));
        (*last)++;
        sem_post(fillCount);
        
        free(prevHdr);
        free(mpegPacket);
        prevHdr = currHdr;
        if (info->seenEnd) {
            break;
        }
    }
    free(info);
    fclose(fp);
    free(cMem);
    free(currHdr);
    free(hdrs);

    return 0;
}

void send_packets(  char *srcport,
                    char *dstip,
                    char *dstport,
                    sem_t *fillCount,
                    sem_t *emptyCount,
                    uint32_t *front,
                    packet_t* buff,
                    int pid,
                    struct sockaddr_in si_all[],
                    int *top_send) {
    /* socket */         
    struct sockaddr_in si_me;
    int s, slen;
    uint16_t port_convert;

    /* child/parent process */
    int status;
    pid_t result;

    /* sleep */
    uint32_t deltaTime;
    struct timespec *req;
    struct timespec *rem;
    struct timeval *tv1;
    struct timeval *tv2;
    uint32_t timeSending;
    struct timespec *semaWait;

    /* loop */
    int i;


    /* set */
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        perror("socket");
        exit(-1);
    }
    req = safe_malloc(sizeof(struct timespec));
    rem = safe_malloc(sizeof(struct timespec));
    tv1 = safe_malloc(sizeof(struct timeval));
    tv2 = safe_malloc(sizeof(struct timeval));
    semaWait = safe_malloc(sizeof(struct timespec));
    slen = sizeof(struct sockaddr_in);
    gettimeofday(tv1, NULL);
    semaWait->tv_sec = tv1->tv_sec + 1;

    /* Set destination parameters */
    memset((char*) &(si_all[0]), 0, sizeof(si_all[0]));
    si_all[0].sin_family = AF_INET;
    port_convert = atoi(dstport);
    si_all[0].sin_port = htons(port_convert);

    if (inet_aton(dstip, &(si_all[0].sin_addr)) == 0) {
        printf("inet_aton() failed\n");
        exit(-1);
    }

    /* Set and bind source parameters */
    memset((char*) &si_me, 0, sizeof(si_me));
    si_me.sin_family = AF_INET;
    port_convert = atoi(srcport);
    si_me.sin_port = htons(port_convert);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);
    if ((bind(s, (struct sockaddr*)&si_me, sizeof(si_me))) == -1) {
        perror("binding socket failed\n");
        exit(-1);
    }

    /* send packets */
    while (true) {
        if (sem_timedwait(fillCount, semaWait) < 0) {
            /* child done making packets - movie ends */
            break;
        }
        for (i = 0; i < *top_send; i++) {
            if (sendto(s,   buff[*front % SHM_NUM_ELEMENTS].packet, 
                            buff[*front % SHM_NUM_ELEMENTS].actualSize,
                            0, (struct sockaddr*)&(si_all[i]), slen) < 0) {
                perror("sendto");
                exit(-1);
            }
            if (i == 0 && *top_send != 1) {
                gettimeofday(tv1, NULL);
            }
        }
        gettimeofday(tv2, NULL);
        deltaTime = buff[*front % SHM_NUM_ELEMENTS].deltaTime;
        (*front)++;
        sem_post(emptyCount);
        if (*top_send != 1 && timeSending > 0) {
        timeSending = (tv2->tv_sec - tv1->tv_sec) * 1000000000;
        timeSending += (tv2->tv_usec - tv1->tv_usec) * 1000;
            if (deltaTime > timeSending) {
                deltaTime -= timeSending;
            }
        }
        req->tv_nsec = (deltaTime);
        semaWait->tv_sec = tv2->tv_sec + 1;
        nanosleep(req, rem);
    }
    close(s);
    free(req);
    free(rem);
    free(tv1);
    free(tv2);
    free(semaWait);
}

/* listens to the manager for clients */
void* receive_packets(void *args) {
    int fd;
    char npipe[MAX_MSG];
    char buff[MAX_MSG];
    char *port;
    uint16_t port_convert;
    struct rp_arg *contain;
    int *top_send;
    struct pollfd fds;
    int nfds = 1;
    int rc;
    int errorCount = 0;

    contain = (struct rp_arg*)args;
    strcpy(npipe, "/tmp/");
    strcat(npipe, contain->filename);
    top_send = contain->top_send;
    if ((fd = open(npipe, O_NONBLOCK)) < 0) {
        perror("receive_packets - open failed\n");
        printf("Continuing to send - Clients can no longer be added\n");
        return NULL;
    } else {
        close(fd);
    }
    if ((fd = open(npipe, O_RDONLY)) < 0) {
        perror("receive_packets - open failed\n");
        printf("Continuing to send - Clients can no longer be added\n");
        return NULL;
    }
    fds.fd = fd;
    fds.events = POLLIN;
    while (*top_send != MAX_CLIENTS) {
        if ((rc = poll(&fds, nfds, -1)) < 0) {
            perror("receive_packets - poll failed\n");
            exit(-1);
        }
        memset(buff, 0, MAX_MSG);
        if (read(fd, buff, MAX_MSG) < 0) {
            perror("packetsender - read failed\n");
            exit(-1);
        }
        if (strlen(buff) == 0) {
            continue;
        }
        if (strcmp(buff, "end") == 0) {
            printf("Got end message\n");
            break;
        }
        port = memchr(buff, ':', MAX_MSG);
        if (port == NULL) {
            printf("receive_packets - null port\n");
            if (++errorCount > 5) {
                printf("Fatal error\n");
                exit(-1);
            }
            continue;
        }
        buff[port - buff] = '\0';
        port++; /* skip the limiter */

        memset( (char*) &(contain->si_all[*top_send]), 
                0, 
                sizeof(struct sockaddr_in));
        contain->si_all[*top_send].sin_family = AF_INET;
        port_convert = atoi(port);
        contain->si_all[*top_send].sin_port = htons(port_convert);
    
        if (inet_aton(buff, &(contain->si_all[*top_send].sin_addr)) == 0) {
            printf("receive_packets - inet_aton() failed\n");
            exit(-1);
        }
        (*top_send)++;
    }

    if (close(fd) < 0) {
        perror("cannot close fd\n");    
    }
    return NULL;
}

int code(char *filename, char *srcport, char *dstip, char *dstport) {
    
    sem_t *fillCount, *emptyCount;
    uint32_t *front, *last;
    packet_t *buff;
    int pid;
    int status;

    int rc_thread;
    pthread_t receive_thread;
    struct rp_arg thread_args;
    int top_send = 1;
    char endmsg[MAX_MSG];
    int fd = -1;
    char pipepath[MAX_MSG];

    fillCount = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE,
                                            MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    if (fillCount == MAP_FAILED) {
        perror("fillCount mmap failed\n");
        exit(-1);
    }
    emptyCount = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE,
                                            MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    if (emptyCount == MAP_FAILED) {
        perror("emptyCount mmap failed\n");
        exit(-1);
    }
    front = mmap(NULL, sizeof(uint32_t), PROT_READ | PROT_WRITE,
                                            MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    if (front == MAP_FAILED) {
        perror("front mmap failed\n");
        exit(-1);
    }
    last = mmap(NULL, sizeof(uint32_t), PROT_READ | PROT_WRITE,
                                            MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    if (last == MAP_FAILED) {
        perror("last mmap failed\n");
        exit(-1);
    }
    buff = mmap(NULL, sizeof(packet_t) * SHM_NUM_ELEMENTS, 
                                        PROT_READ | PROT_WRITE,
                                        MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    
    if (buff == MAP_FAILED) {
        perror("buff mmap failed\n");
        exit(-1);
    }

    sem_init(fillCount, 1, 0);
    sem_init(emptyCount, 1, SHM_NUM_ELEMENTS);

    pid = fork();

    if (pid < 0) {
        printf("fork failed\n");
        exit(-1);
    } else if (pid == 0) { /* child */
        produce_packets(filename, buff, fillCount, emptyCount, last);
        exit(0);
    }

    thread_args.top_send = &top_send;
    strcpy(thread_args.filename, filename);

    rc_thread = pthread_create( &receive_thread, 
                                NULL,
                                receive_packets,
                                &thread_args);   
    if (rc_thread < 0) {
        printf("failed to create new thread\n");
    }
    /* parent */
    send_packets(   srcport,    dstip,  dstport,    fillCount,  emptyCount, 
                    front,      buff,   pid, thread_args.si_all,&top_send); 
 

    if (wait(&status) < 0) {
        printf("wait failed\n");
    }

    /* clean up */
    memset(pipepath, 0, MAX_MSG);
    strcpy(pipepath, "/tmp/");
    strcat(pipepath, filename);
    memset(endmsg, 0, MAX_MSG);
    strcpy(endmsg, "end");
    if ((fd = open(pipepath, O_NONBLOCK)) < 0) {
        /* thread could not add more than one client*/
        perror("cold not open\n");
    } else {
        close(fd);   
        if ((fd = open(pipepath, O_WRONLY)) < 0) {
            perror("packetsender cleanup - open failed\n");
        } else {
            write(fd, endmsg, MAX_MSG);
            close(fd);
        }
    }
    if (pthread_join(receive_thread, NULL) < 0) {
        printf("could not join thread\n");
    }
    printf("thread joined\n");
    memset(pipepath, 0, MAX_MSG);
    strcpy(pipepath, "/tmp/manager");
    memset(endmsg, 0, MAX_MSG);
    strcpy(endmsg, filename);
    if ((fd = open(pipepath, O_NONBLOCK)) < 0) {
        /* thread could not add clients*/
        printf("cannot open pipepath - pipepath is |%s|\n", pipepath);
    } else {
        close(fd);   
        if ((fd = open(pipepath, O_WRONLY)) < 0) {
            perror("packetsender cleanup send - open failed\n");
        } else {
            write(fd, endmsg, MAX_MSG);
            close(fd);
            printf("done writing\n");
        }
    }

    
    munmap(fillCount, sizeof(sem_t));
    munmap(emptyCount, sizeof(sem_t));
    munmap(front, sizeof(uint32_t));
    munmap(last, sizeof(uint32_t));
    munmap(buff, sizeof(packet_t) * SHM_NUM_ELEMENTS);

    printf("packetsender |%s| ended\n", filename);

    return 0;
}

int main(int argc, char* argv[]) {
    int i;
    if (argc != 5) {
        printf("Must input file name, src port, dst ip and dst port\n");
        return -1;
    }

    srand(time(NULL));

    code(argv[1], argv[2], argv[3], argv[4]);
    printf("exitting\n");
    return 0;
}
