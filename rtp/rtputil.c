#include "rtputil.h"
#include "rtpfile.h"

void* safe_malloc(size_t size) {
    void *mem = calloc(size, 1);
    if (mem == NULL) {
        perror("Error in safe_malloc, mem is NULL\n");
        exit(-1);
    }
    return mem;
}

void realloc_cMem(FILE *fp, mem_t *cMem) {
    int32_t offset;

    offset = -(cMem->maxSize - cMem->memTraversed);

    loadFiletoMem(fp, offset, SEEK_CUR, cMem);
}

void* make_mem_contain_f() {
    mem_t *cMem;

    cMem = safe_malloc(sizeof(mem_t));

    cMem->maxSize = MEMORY_BUFFER_SIZE;

    return cMem;
}

void* make_mem_contain() {
    mem_t *cMem;

    cMem = safe_malloc(sizeof(mem_t));

    cMem->maxSize = MAX_MPEG_PAYLOAD_SIZE;

    return cMem;
}

void reset_mem(mem_t *cMem) {
    memset(cMem->mem, 0, cMem->maxSize);
    cMem->memTraversed = 0;
}

/* Insert value in data in big endian format */
uint32_t changeEndian(uint32_t value) {
    uint32_t newValue;
    int i;

    for (i = 24; i >= 0; i -= 8) {
        newValue |= ((value >> i) & ((8 << 1) - 1)) << (24 - i);
    }
    return newValue;
}
