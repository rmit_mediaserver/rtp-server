#ifndef _JPM_MOD_H_
#include <linux/ioctl.h>
#define JPM_DEVICEMAJOR 240
#define IOCTL_DOWN_SEM _IOR(JPM_DEVICEMAJOR, 0, int)
#define IOCTL_UP_SEM _IOR(JPM_DEVICEMAJOR, 1, int)
#define _JPM_MOD_H
#endif
