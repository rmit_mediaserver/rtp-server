#include "rtputil.h"
#include "rtpfile.h"
#include "rtpframe.h"
#include "rtpmpegframe.h"
#include "rtpfileparser.h"
#include "packetsender.h"

#include <sys/time.h>
#include <sys/ioctl.h>

#include <arpa/inet.h>
#define PORT_OTHER  9930
#define PORT_ME     9930
#define SRV_IP "192.168.1.244"

#include "jpm_mod.h"

int produce_packets(char *filename, char *addrport) {
    FILE *fp;
    mem_t *cMem;   
    hdrs_t *hdrs;
    rtp_hdr_t *prevHdr;
    rtp_hdr_t *currHdr;
    rtp_info_t *info;
    mpeg_packet_t *mpegPacket;
    uint32_t last;
    uint32_t u_sec;
    int fd;
    packet_t *shmMem;
    
    fd = open("/dev/jpm_sender0", O_RDWR); 
    if (fd < 0) {
        printf("open failed\n");
        exit(-1);
    }
    fp = openMPEG(filename);
    hdrs = safe_malloc(sizeof(hdrs_t));
    cMem = safe_malloc(sizeof(mem_t));
    info = safe_malloc(sizeof(rtp_info_t));
    prevHdr = NULL;
    last = 0;

    shmMem = mmap(  NULL, 
                    sizeof(packet_t) * SHM_NUM_ELEMENTS,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    fd,
                    0);
    if (write(fd, addrport, 16) < 0) {
        printf("writing to kernel node failed\n");
        exit(-1);
    }

    loadFiletoMem(fp, 0, SEEK_SET, cMem);
    
    while(true) {
        mpegPacket = createMPEGPacket(fp, cMem, hdrs, info);

        if (info->sample == 3003) { 
            u_sec = 33366700;
        } else {
            printf("this samplerate isn't supoprted\n");
        }

        currHdr = createRTPHeader(prevHdr, info);
        free(prevHdr);

        if (ioctl(fd, IOCTL_DOWN_SEM, 0) < 0) {
            printf("sender has stopped sending\n");
            free(mpegPacket);
            break;
        }
        shmMem[(last % SHM_NUM_ELEMENTS)].actualSize =
                    info->mpegPayloadSize + sizeof(rtp_hdr_t) + 
                                                sizeof(mpeg_vid_hdr_t);
        shmMem[(last % SHM_NUM_ELEMENTS)].deltaTime = 
                    info->changedFrame?u_sec:0;
        memcpy(shmMem[(last % SHM_NUM_ELEMENTS)].packet, 
                                            currHdr, sizeof(rtp_hdr_t));
        memcpy(shmMem[(last % SHM_NUM_ELEMENTS)].packet + sizeof(rtp_hdr_t), 
                                            mpegPacket, sizeof(mpeg_packet_t));
        last++;
        if (ioctl(fd, IOCTL_UP_SEM, 0) < 0) {
            printf("sender has stopped sending\n");
            free(mpegPacket);
            break;
        }


        
        free(mpegPacket);
        prevHdr = currHdr;
        if (info->seenEnd) {
            break;
        }
    }
    close(fd);
    fclose(fp);
    free(info);
    free(cMem);
    free(currHdr);
    free(hdrs);

    return 0;
}

int main(int argc, char* argv[]) {
    srand(time(NULL));

    if (argc != 3) {
        printf("requires filename and address:port\n");
        printf("address needs to be in hexadecimal format\n");
        exit(-1);
    }

    return produce_packets(argv[1], argv[2]);
}
