#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

int main (int argc, char *argv[]) {
    int fd;

    if (argc != 2) {
        printf("requires address:port\n");
        printf("address needs to be in hexadecimal format\n");
        exit(-1);
    }

    fd = open("/dev/jpm_sender0", O_RDWR);

    if (fd < 0) {
        perror("open failed\n");
        exit(-1);
    }

    if (write(fd, argv[1], 16) < 0) {
        printf("Writing to kerne node failed\n");
        exit(-1);
    }
    return 0;
}
