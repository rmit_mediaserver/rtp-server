#include "rtputil.h"
#include "rtpfile.h"
#include "rtpframe.h"
#include "rtpmpegframe.h"
#include "rtpfileparser.h"

#include <arpa/inet.h>

#define NPACK 10
#define PORT 9930
#define SRV_IP "192.168.1.244"

void code () {
    struct sockaddr_in si_other;  
    int s, slen, i;
    int buffSize = MIN_RTP_HEADER_SIZE +
                MAX_MPEG_HEADER_SIZE + MAX_MPEG_PAYLOAD_SIZE;
    uint8_t buf[MIN_RTP_HEADER_SIZE +
                MAX_MPEG_HEADER_SIZE + MAX_MPEG_PAYLOAD_SIZE];
    
    FILE *fp;
    mem_t *cMem;   
    hdrs_t *hdrs;
    rtp_hdr_t *prevHdr;
    rtp_hdr_t *currHdr;
    rtp_info_t *info; 
    mpeg_packet_t *mpegPacket;
    struct timespec *req;
    struct timespec *rem;

    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        perror("socket");
        exit(-1);
    }

    if (inet_aton(SRV_IP, &si_other.sin_addr) == 0) {
        printf("inet_aton() failed\n");
        exit(-1);
    }


    fp = openMPEG("centaur_1.mpg");
    hdrs = safe_malloc(sizeof(hdrs_t));
    cMem = safe_malloc(sizeof(mem_t));
    info = safe_malloc(sizeof(rtp_info_t));
    req = safe_malloc(sizeof(struct timespec));
    rem = safe_malloc(sizeof(struct timespec));
    prevHdr = NULL;
    loadFiletoMem(fp, 0, SEEK_SET, cMem);

    req->tv_nsec = (5 * 1000000);

  for (i = 0; i < 2000; i++) {
    /*printf("%u\n", i);*/
    mpegPacket = createMPEGPacket(fp, cMem, hdrs, info);
    
    currHdr = createRTPHeader(prevHdr, info);
    free(prevHdr);

    memcpy(buf, currHdr, sizeof(rtp_hdr_t));
    memcpy(buf + sizeof(rtp_hdr_t), mpegPacket, sizeof(mpeg_packet_t));
    
    buffSize = info->mpegPayloadSize +  sizeof(rtp_hdr_t) + 
                                        sizeof(mpeg_vid_hdr_t);
    slen = sizeof(si_other);

    memset ((char*) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);

    if (sendto(s, buf, buffSize, 0, (struct sockaddr*)&si_other, slen) == -1) {
        perror("sendto");
        exit(-1);
    }
    free(mpegPacket);
    prevHdr = currHdr;
    if (cMem->maxSize != MEMORY_BUFFER_SIZE) {
        printf("Near the end\n");
        break;
    }
    nanosleep(req, rem);
  }
    printf("done\n");
    close(s);
    free(info);
    fclose(fp);
    free(cMem);
    free(currHdr);
    free(hdrs);
    free(req);
    free(rem);

}

void test() {

}

int main(int argc, char* argv[]) {
    srand(time(NULL));
    code();

    return 0;
}
