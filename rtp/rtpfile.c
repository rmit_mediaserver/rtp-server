#include "rtpfile.h"

/* safe fopen of file */
FILE* openMPEG(char* filePath) {
    FILE *fp = fopen(filePath, "rb");
    if (fp == NULL) {
        perror("openMPEG - File pointer in MPEG is null\n");
        printf("Unable to open |%s|\n", filePath);
        exit(-1);
    }
    return fp;
}

/* loads a file section (governed by offset and origin) into
 * memory buffer cMem
 */
void loadFiletoMem(     FILE *fp,
                        long int offset, 
                        int origin, 
                        mem_t *cMem) {
    uint32_t bytesRead;

    if (cMem == NULL) {
        printf("loadFiletoMem - cMem null\n");
        exit(-1);
    }
    
    if (fseek(fp, offset, origin) != 0) {
        perror("loadFiletoMem - fseek is non-zero\n");
        exit(-1);
    }

    if ((bytesRead = fread(cMem->mem, 1, MEMORY_BUFFER_SIZE, fp)) 
                                                != MEMORY_BUFFER_SIZE) {
        if (!feof(fp)) {
            perror("loadFiletoMem - Error in fread\n");
            exit(-1);
        }
    }
    cMem->maxSize = bytesRead;
    cMem->memTraversed = 0;
}
