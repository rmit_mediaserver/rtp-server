#ifndef _H_PACKET_SENDER_

#define SHM_NUM_ELEMENTS 10

typedef struct contain_packet packet_t;

struct contain_packet {
    uint32_t actualSize;
    uint32_t deltaTime;
    uint8_t packet[MAX_PACKET_SIZE];
};
#define _H_PACKET_SENDER_
#endif
