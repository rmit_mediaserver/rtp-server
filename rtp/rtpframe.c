#include "rtpframe.h" 

void populateRTPHeader( rtp_hdr_t *hdr,
                        rtp_hdr_t *prevHdr,
                        rtp_info_t *info) {
    uint32_t tempRow;

    if (hdr == NULL || info == NULL) {
        perror("populateRTPHeader - Invalid parameters\n");
        exit(-1);
    }

    memset(hdr, 0, sizeof(rtp_hdr_t));
    tempRow = 0;

    hdr->data[0] |= RTP_VERSION_NUMBER << 6;
    hdr->data[0] |= 0 << 5;
    hdr->data[0] |= 0 << 4;
    hdr->data[0] |= 0 << 0;
    hdr->data[1] |= 0 << 7;
    hdr->data[1] |= RTP_MPEG_VIDEO_PROFILE << 0;

    if (prevHdr != NULL && info != NULL) {
        tempRow = 0;
        tempRow  = prevHdr->data[2] << 8;
        tempRow |= prevHdr->data[3] << 0;
        tempRow = (tempRow + 1) % 65535;

        hdr->data[2] = (tempRow >> 8);
        hdr->data[3] |= (tempRow << 0) & ((1 << 8) - 1);
        if (info->numFrames == 0) {
            tempRow = info->timestampStart;          
        } else {
            tempRow = (info->timestampStart + 
                        (info->sample * info->numFrames-1)) % 4294967295U;
            info->changedFrame = info->prevFrames != info->numFrames;
        }
        info->prevFrames = info->numFrames;
        hdr->data[4] = (tempRow >> 24) & ((1 << 8) -1);
        hdr->data[5] = (tempRow >> 16) & ((1 << 8) -1);
        hdr->data[6] = (tempRow >> 8) & ((1 << 8) -1);
        hdr->data[7] = (tempRow >> 0) & ((1 << 8) -1);

        hdr->data[8] = prevHdr->data[8];
        hdr->data[9] = prevHdr->data[9];
        hdr->data[10] = prevHdr->data[10];
        hdr->data[11] = prevHdr->data[11];
    } else {
        /* sync source */
        tempRow = (rand() % 65535);
        hdr->data[2] = tempRow >> 8;
        hdr->data[3] = tempRow & ((8 << 1)-1);

        /* timestamp */
        tempRow = (rand() % 4294967295U);
        hdr->data[4] = (tempRow >> 24) & ((1 << 8) -1);
        hdr->data[5] = (tempRow >> 16) & ((1 << 8) -1);
        hdr->data[6] = (tempRow >> 8) & ((1 << 8) -1);
        hdr->data[7] = (tempRow >> 0) & ((1 << 8) -1);
        info->timestampStart = tempRow;

        hdr->data[9] = (tempRow >> 16) & ((1 << 8) -1);
        hdr->data[10] = (tempRow >> 8) & ((1 << 8) -1);
        hdr->data[11] = (tempRow >> 0) & ((1 << 8) -1);
    }
}

rtp_hdr_t* createRTPHeader (    rtp_hdr_t *prevHdr,
                                rtp_info_t *info) {
    rtp_hdr_t *newHdr;

    newHdr = safe_malloc(sizeof(rtp_hdr_t));
    
    populateRTPHeader(newHdr, prevHdr, info);

    return newHdr;
}

