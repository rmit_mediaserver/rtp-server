#ifndef _RTP_HEADER_

#define _POSIX_C_SOURCE 200112L
#define _BSD_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <semaphore.h>
#include <netinet/in.h>
#include <time.h>
#include <features.h>

/* ---- IN UNITS OF BYTES ---- */
#define MEMORY_BUFFER_SIZE      2000    /* File to memory buffer */ 
#define MIN_MPEG_HEADER_SIZE    4       
#define MAX_MPEG_HEADER_SIZE    4       
#define MIN_RTP_PAYLOAD_SIZE    261     /* Min. MPEG hdr + Min. MPEG payload */
#define MAX_MPEG_PAYLOAD_SIZE   1600     
#define MIN_RTP_HEADER_SIZE     12 
#define RTP_HEADER_SIZE         16 
#define MAX_PACKET_SIZE         MIN_RTP_HEADER_SIZE + \
                                MAX_MPEG_HEADER_SIZE + \
                                MAX_MPEG_PAYLOAD_SIZE
/* ---- END ---- */

#define OCTET_SIZE              8       /* bits */
#define RTP_VERSION_NUMBER      2
#define RTP_MPEG_AUDIO_PROFILE  14
#define RTP_MPEG_VIDEO_PROFILE  32

#define MPEG_SEQ_HDR_CODE       0xB3
#define MPEG_GOP_HDR_CODE       0xB8
#define MPEG_PIC_HDR_CODE       0x00
#define MPEG_END_HDR_CODE       0xB7

#define MPEG_CODE_PREFIX_SIZE   4       /* bytes */

#define MPEG_MBZ_BIT_SHIFT      27
#define MPEG_T_BIT_SHIFT        26
#define MPEG_TR_BIT_SHIFT       16
#define MPEG_AN_BIT_SHIFT       15
#define MPEG_N_BIT_SHIFT        14
#define MPEG_SEQ_BIT_SHIFT      13
#define MPEG_B_BIT_SHIFT        12
#define MPEG_E_BIT_SHIFT        11
#define MPEG_P_BIT_SHIFT        8
#define MPEG_FBV_BIT_SHIFT      7
#define MPEG_BFC_BIT_SHIFT      4
#define MPEG_FFV_BIT_SHIFT      3
#define MPEG_FFC_BIT_SHIFT      0

#define SEQ_PR_BIT_SHIFT        28
#define SEQ_AR_BIT_SHIFT        24 
#define SEQ_VSIZE_BIT_SHIFT     12
#define SEQ_HSIZE_BIT_SHIFT     0

#define SEQ_LQ_BIT_SHIFT        30
#define SEQ_C_BIT_SHIFT         29
#define SEQ_BS_BIT_SHIFT        19
#define SEQ_MARKER_BIT_SHIFT    18
#define SEQ_BR_BIT_SHIFT        0

#define GOP_BL_BIT_SHIFT        26
#define GOP_CG_BIT_SHIFT        25
#define GOP_P_BIT_SHIFT         19
#define GOP_S_BIT_SHIFT         13
#define GOP_MARKER_BIT_SHIFT    12
#define GOP_M_BIT_SHIFT         6
#define GOP_H_BIT_SHIFT         1
#define GOP_DF_BIT_SHIFT        0

#define PIC_DELAY_BIT_SHIFT     13
#define PIC_CT_BIT_SHIFT        10
#define PIC_TR_BIT_SHIFT        0

#define PIC_BC_BIT_SHIFT        5
#define PIC_BV_BIT_SHIFT        4
#define PIC_FC_BIT_SHIFT        1
#define PIC_FV_BIT_SHIFT        0

#define PIC_C_MASK              ((1 << 3) - 1)
#define PIC_V_MASK              ((1 << 1) - 1)
#define PIC_CT_MASK             ((1 << 3) - 1)
#define PIC_TR_MASK             ((1 << 10) - 1)
#define SEQ_PR_MASK             ((1 << 4) - 1)

#define RTP_VERSION_BIT_SHIFT   30
#define RTP_PADDING_BIT_SHIFT   29
#define RTP_EXTENSION_BIT_SHIFT 28
#define RTP_CC_COUNT_BIT_SHIFT  24
#define RTP_MARKER_BIT_SHIFT    23
#define RTP_PAYLOAD_BIT_SHIFT   16
#define RTP_SEQUENCE_BIT_SHIFT  0

#define RTP_SEQ_MASK       ((1 << 16) - 1)

typedef int bool;
enum {false, true};

typedef struct memory {
    uint8_t mem[MEMORY_BUFFER_SIZE];
    uint32_t memTraversed;
    uint32_t maxSize;
} mem_t;

/* Safe version of malloc */
void* safe_malloc(size_t);
/* Gets the next chunk of memory from the file to cMem */
void realloc_cMem(FILE*, mem_t*);
/* Create a memory container */
void* make_mem_contain();
/* Create a file memory container */
void * make_mem_contain_f();
/* Reset the values of a memory container */
void reset_mem(mem_t*);
uint32_t changeEndian(uint32_t);

#define _RTP_HEADER_
#endif
