#include "rtpfileparser.h"

/* 0xB3 if video sequence header
 * 0xB7 if end video sequence
 *  1   if slice is sequential slice
 *  0   if slice header found is a new picture (picture header)
 * -1   if realloc is necessary
 * -2   if dataBuff is too small
 */ 
int findNextChunk(      mem_t *cMem,                 /* Memory */
                        mem_t *cData,                /* Data Buffer*/
                        bool metSlice,            /* 1 to return hdr */
                        uint8_t *currSliceCode) {  
    uint32_t ptrb;
    int b_payloadSize;
    uint8_t octet;
    
    if (cMem == NULL || cData == NULL || currSliceCode == NULL) {
        printf("findNextSlice - One or more parameters NULL\n");
        exit(-1);
    }
    
    ptrb = 0;
    
    /* Need to find 00 00 01 XX slice */
    ptrb = findNextMPEGHeader(cMem, ptrb);


    if (ptrb == -1 || ptrb == -3) {
        return ptrb;
    } 
    /* Not sure about this corner case */
    if (cMem->memTraversed + ptrb + 4 > cMem->maxSize ) {
        return -1;
    }
    /* Check last octet for data header type */
    octet = (cMem->mem)[cMem->memTraversed + ptrb + 3]; 

    if (metSlice &&    (octet == MPEG_SEQ_HDR_CODE ||
                        octet == MPEG_GOP_HDR_CODE ||
                        octet == MPEG_END_HDR_CODE)) { 
        /* found header when looking for slice */
        return octet;
    } else if (octet - 1  == *currSliceCode) {
        octet = 1;
    } 

    /* Payload */
    b_payloadSize = findNextMPEGHeader( cMem, 
                                        ptrb + MPEG_CODE_PREFIX_SIZE);
    
    if (b_payloadSize == -1) {
        return -1;
    } else {
        if (cData->maxSize <  b_payloadSize) {
            return -2;
        }
        if (b_payloadSize == -3) {
            cData->memTraversed = cMem->maxSize - cMem->memTraversed;
        } else {
            cData->memTraversed = b_payloadSize;
        }
        memcpy(cData->mem, (cMem->mem) + cMem->memTraversed, b_payloadSize);
        if (octet < 0x10) {
            *currSliceCode = octet;
        }   
        cMem->memTraversed += ptrb;   /*Skip the ignored bits */
    }

    return octet;
}

/* Returns the number of bytes skipped to the next 00 00 01
 * >= 0 if 00 00 01 found
 *   -1 if accessing outside memory AKA not found within memory
 *   -3 if end of file reached
 */
int findNextMPEGHeader(    mem_t *cMem, 
                           uint32_t offsetb ) {
    uint32_t areaCovered;
    uint8_t headerValues[3] = {0x00, 0x00, 0x01};
    uint8_t *newPtrAddressb;
    int i;
    bool found;
    
    areaCovered = 0;
    found = false;
    
    while (!found) {
        newPtrAddressb = 
                    (uint8_t*)memchr(   cMem->mem + cMem->memTraversed + 
                                                    offsetb + areaCovered,
                                        headerValues[0], 
                                        cMem->maxSize - cMem->memTraversed - 
                                                    offsetb - areaCovered);
        if (newPtrAddressb == NULL) {
            if (cMem->maxSize != MEMORY_BUFFER_SIZE) {
                /* end of file */
                printf("end of file\n");
                return -3;
            } else {
                return -1;
            }
        }
        for (i = 1; i < 3; i++) {
            if (newPtrAddressb[i] != headerValues[i]) {
                i = 100;  /* Break, no match */
            }
        }

        areaCovered = newPtrAddressb - (cMem->mem + cMem->memTraversed) - 
                                                    offsetb + 1;

        if (i > 5) {
            found = false;
        } else {
            found = true;
        }
    }
    return newPtrAddressb - (cMem->mem + cMem->memTraversed);
}

