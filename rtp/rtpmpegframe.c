#include "rtpmpegframe.h"

void populateMPEGHeader(    mpeg_vid_hdr_t *mpegHeader,
                            hdrs_t *hdr) {

    uint32_t tr, ct;
    uint32_t temp;

    if (hdr->cPic.mem == NULL || mpegHeader == NULL) {
        printf("packMPEGHeader - One or more parameters are NULL\n");
        exit(-1);
    }

    memset(mpegHeader, 0, sizeof(mpeg_vid_hdr_t));
    ct =(((pic_hdr_t*)(hdr->cPic.mem))->data[5] >> 3) & ((1 << 3) - 1);
    tr =    (((pic_hdr_t*)(hdr->cPic.mem))->data[5] >> 6) | 
            (((pic_hdr_t*)(hdr->cPic.mem))->data[4] << 8);
    temp = 0;
                       /* Value      #bits        #skipped */
    mpegHeader->data[0] |= (0 & ((1 << 4) - 1))  << 3;
    mpegHeader->data[0] |= (0 & ((1 << 1) - 1))  << 2;
    mpegHeader->data[0] |= (tr >> 8) << 0;
    mpegHeader->data[1] |= (tr & ((1 << 8) - 1))  << 0; 
    mpegHeader->data[2] |= (0 & ((1 << 1) - 1))  << 7; 
    mpegHeader->data[2] |= (0 & ((1 << 1) - 1))  << 6;                 
    mpegHeader->data[2] |= (hdr->seenSeq & ((1 << 1) - 1))  << 5; 
    mpegHeader->data[2] |= (1 & ((1 << 1) - 1))  << 4;                 
    mpegHeader->data[2] |= (0 & ((1 << 1) - 1))  << 3; 
    mpegHeader->data[2] |= (ct & ((1 << 3) - 1))  << 0; 
    if (ct == 2 || ct == 3) {
        temp = (((pic_hdr_t*)(hdr->cPic.mem))->data[7] >> 2) & ((1 << 1)-1); 
        mpegHeader->data[3] |= (temp & ((1 << 1) - 1))  << 3; 

        temp =((((((pic_hdr_t*)(hdr->cPic.mem))->data[6] >> 0) & ((1 << 2)-1)))
                                                            << 1) |
              (((((pic_hdr_t*)(hdr->cPic.mem))->data[8] >> 7) & ((1 << 1)-1)));
        mpegHeader->data[3] |= (temp & ((1 << 3) - 1))  << 0; 
    }
    if (ct == 3) {
        temp = (((pic_hdr_t*)(hdr->cPic.mem))->data[8] >> 6) & ((1 << 1)-1); 
        mpegHeader->data[3] |= (temp & ((1 << 1) - 1))  << 7; 
        temp = (((pic_hdr_t*)(hdr->cPic.mem))->data[8] >> 3) & ((1 << 3)-1); 
        mpegHeader->data[3] |= (temp & ((1 << 3) - 1))  << 4; 
    }
}

/* Return 0 if sucessful
 * Return -1 if not enough space in buffer
 */

int packChunk(  mem_t *cMem,                 
                mem_t *cMpegPayload,         /* Payload goes in here */
                mem_t *rtpChunk ){           /* Payload */

    if (cMpegPayload->maxSize < rtpChunk->memTraversed + 
                                        cMpegPayload->memTraversed) {
        return -1;
    }

    /* For the MPEG payload */
    memcpy( cMpegPayload->mem + cMpegPayload->memTraversed, 
            rtpChunk->mem, 
            rtpChunk->memTraversed);
    cMpegPayload->memTraversed += rtpChunk->memTraversed;
    
    if (cMem != NULL) {
        /* For the file data */
        cMem->memTraversed += rtpChunk->memTraversed; 
    }
    
    return 0;
}

void injectHeaders (    mem_t *cMpegPayload,
                        hdrs_t *injects,
                        int returnCode ) {

    mem_t ptrs[3];
    int i, end;
    
    ptrs[0] = injects->cSeq; 
    ptrs[1] = injects->cGOP; 
    ptrs[2] = injects->cPic;
    end = 0;

    switch (returnCode) {
        case 0xB3: end = 3; break;
        case 0xB8: end = 2; break;
        case 0x00: end = 1; break;
        case -1: 
            printf("injectHeaders - negative one\n");
            exit(-1);
        case -2:
            printf("injextHeaders - negative two\n");
            exit(-1);
        default: end = 0; break;
    }

    for (i = 0; i < 3 - end; i++) {
        if (ptrs[i].memTraversed != 0) {
            if (packChunk(NULL, cMpegPayload, &ptrs[i]) == -1) {
                printf("injectHeaders - packChunk w/ %d\n", i);
                printf("no space in empty packet?\n");
                exit(-1);
            }
        }
    }
}

mem_t* createPackedMPEGPayload( FILE *fp,
                                mem_t *cMem,
                                hdrs_t *hdrs ){
    mem_t *cMpegPayload;
    mem_t *cDataBuff;
    uint8_t currSliceCode;
    int returnCode;
    bool metSlice;
    bool isPacked;
    int lockCheck;

    cMpegPayload = make_mem_contain();
    cDataBuff = make_mem_contain();
    currSliceCode = 0;
    returnCode = 0;
    metSlice = false;
    isPacked = false;
    hdrs->seenSeq = false;
    hdrs->seenGOP = false;
    hdrs->seenPic = false;
    hdrs->seenEnd = false;
    lockCheck = 0;

    returnCode = findNextChunk(cMem, cDataBuff, metSlice, &currSliceCode);

    if (returnCode == -1) {
        realloc_cMem(fp, cMem);
        returnCode = findNextChunk(cMem, cDataBuff, metSlice, &currSliceCode);
    } else if (returnCode == MPEG_END_HDR_CODE || returnCode == -3) {
        hdrs->seenEnd = true;
        free(cDataBuff);
        return cMpegPayload;
    }

    /*injectHeaders(cMpegPayload, hdrs, returnCode);*/
    
    while (!isPacked) {
        if (metSlice && (   returnCode == MPEG_SEQ_HDR_CODE || 
                            returnCode == MPEG_GOP_HDR_CODE ||
                            returnCode == MPEG_PIC_HDR_CODE ||
                            returnCode == MPEG_END_HDR_CODE ||
                            returnCode == -3)) {
            isPacked = true;
            if (returnCode == MPEG_END_HDR_CODE || returnCode == -3) {
                hdrs->seenEnd = true;
            }
            continue;
        }
        if (returnCode == MPEG_SEQ_HDR_CODE) {
            memcpy(&(hdrs->cSeq), cDataBuff, sizeof(mem_t));
            hdrs->seenSeq = true;
        } else if (returnCode == MPEG_GOP_HDR_CODE) {
            memcpy(&(hdrs->cGOP), cDataBuff, sizeof(mem_t));
            hdrs->seenGOP = true;
        } else if (returnCode == MPEG_PIC_HDR_CODE) {
            memcpy(&(hdrs->cPic), cDataBuff, sizeof(mem_t));
            hdrs->seenPic = true;
            hdrs->numPicHdr++;
        } else if (returnCode == -1) {
            if (lockCheck > 2) {
                printf("rtpmpegframe - lockCheck active\n");
                printf("increase MEMORY_BUFFER_SIZE\n");
                exit(-1);
            }
            realloc_cMem(fp, cMem);
            returnCode = findNextChunk(cMem, cDataBuff, 
                                        metSlice, &currSliceCode);
            lockCheck++;
            continue;
        } else if(returnCode == -2) {
            printf("rtpmpegframe.c - data buff too small\n");
            printf("please increase MAX_MPEG_PAYLOAD_SIZE\n");
            exit(-1);
        } else {
            metSlice = true;
        }
        if (packChunk(cMem, cMpegPayload, cDataBuff) == -1) {
            isPacked = true;
            continue;
        }

        cDataBuff->memTraversed = 0;

        returnCode = findNextChunk(cMem, cDataBuff, 
                                        metSlice, &currSliceCode);
    }
    free(cDataBuff);

    return cMpegPayload;
}

/* Get info for RTP header */
void fillMPEGinfo ( rtp_info_t *info,
                    hdrs_t *hdrs,
                    mem_t *cMpegPayload){
    uint32_t samples[9] = {0, 3753, 3750, 3600, 3003, 3000, 1800, 1502, 1500 };
    double framerates[9] = {0, 23.976, 24, 25, 29.97, 30, 50, 59.94, 60};
    uint32_t index;
    if (info == NULL || hdrs == NULL || cMpegPayload == NULL) {
        printf("fillMPEGinfo - Invalid parameters\n");
        exit(-1);
    }

    index = 
        ((((seq_hdr_t*)(hdrs->cSeq.mem))->data[7]) >> 0) & ((1 << 4)-1);

    if (index == 0 || index >= 9) {
        printf("fillMPEGinfo - Invalid picture rate\n");
        exit(-1);
    }
     /* NO PADDING FOR NOW 
    info->paddingSize = MAX_MPEG_PAYLOAD_SIZE - cMpegPayload->memTraversed;
    if (info->paddingSize) {
        (cMpegPayload->mem)[MAX_MPEG_PAYLOAD_SIZE - 1] 
                                                = info->paddingSize;
    }
    */
    info->sample = samples[index];
    info->framerate = framerates[index];
    info->mpegPayloadSize = cMpegPayload->memTraversed;
    info->numFrames = hdrs->numPicHdr - 1;
    info->seenEnd = hdrs->seenEnd;
}

/* Returns a MPEGPacket 
 */
mpeg_packet_t *createMPEGPacket (   FILE *fp,
                                    mem_t *cMem,
                                    hdrs_t *hdrs,
                                    rtp_info_t *info) {
    mpeg_packet_t *mpegPacket;
    mem_t *cPayload;

    mpegPacket = safe_malloc(sizeof(mpeg_packet_t));
    cPayload = createPackedMPEGPayload(fp, cMem, hdrs);

    populateMPEGHeader(&(mpegPacket->hdr), hdrs);
    /* Insert the payload */
    memmove(mpegPacket->payload, cPayload->mem, cPayload->memTraversed);

    fillMPEGinfo(info, hdrs, cPayload);

    free(cPayload);

    return mpegPacket;
}
