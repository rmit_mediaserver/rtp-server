#ifndef _RTP_FILE_PARSER_H
#include "rtputil.h"

/* Gets the next chunk of data from the file memory buffer. Contains
 * the header sequence and the payload */
int findNextChunk (mem_t*, mem_t*, bool, uint8_t*);
/* Returns the number of bytes from the memory pointer to the next 
 * sequence header */
int findNextMPEGHeader(mem_t*, uint32_t);
#define _RTP_FILE_PARSER_H
#endif
