#ifndef _RTP_MPEG_FRAME_H_
#include "rtputil.h"
#include "rtpframe.h"
#include "rtpfileparser.h"
#include "rtpfile.h"
typedef struct mpeg_packet mpeg_packet_t;
typedef struct mpeg_hdrs hdrs_t;
typedef struct mpeg_vid_header mpeg_vid_hdr_t;
typedef struct seq_header seq_hdr_t;
typedef struct GOP_header GOP_hdr_t;
typedef struct pic_header pic_hdr_t;
typedef struct audio_header audio_hdr_t;

struct mpeg_hdrs {
    mem_t cSeq;
    mem_t cGOP;
    mem_t cPic;
    bool seenSeq;
    bool seenGOP;
    bool seenPic;
    bool seenEnd;
    uint32_t numPicHdr;
};

struct mpeg_vid_header {
    uint8_t data[4];
};

struct seq_header {
    uint8_t data[28];
    /*
    uint32_t code;
    uint32_t midOne;         sizeh, sizev, aspect ratio, picutre rate
    unsigned int midTwo:31;
    uint64_t mPartOne;
    unsigned int lnq:1;
    uint64_t mPartTwo;
    */
};

struct GOP_header {
    uint8_t data[8];
    /*
    uint32_t code;
    unsigned int mid:27;
    */
};

struct pic_header {
    uint8_t data[9];
    /*
    uint32_t code;
    unsigned int mid:29;
    uint8_t pb;
    */
};

struct mpeg_audio_header {
    uint16_t mbz;
    uint16_t frag;
};

struct mpeg_packet {
    mpeg_vid_hdr_t hdr;
    uint8_t payload[MAX_MPEG_PAYLOAD_SIZE];
};

/* Fills an MPEG header according to parameters */
void populateMPEGHeader(mpeg_vid_hdr_t*, hdrs_t*);
/* Packs a chunk of memory into payload and advance pointers
 * in the container for the payload and file memory */
int packChunk(mem_t*, mem_t*, mem_t*);
/* Injects headers into the payload depending on the returnCode
 * that is supplied */
void injectHeaders(mem_t*, hdrs_t*, int);
/* Fills the MPEG payload with data from file memory buffer */
mem_t* createPackedMPEGPayload(FILE*, mem_t*, hdrs_t*);
/* Saves the necessary information to populate the RTP header */
void fillMPEGinfo(rtp_info_t*, hdrs_t*, mem_t*);
/* Creates a full MPEG packet with complete MPEG headers and payload */
mpeg_packet_t* createMPEGPacket(FILE*, mem_t*, hdrs_t*, rtp_info_t*);
#define _RTP_MPEG_FRAME_H_
#endif
