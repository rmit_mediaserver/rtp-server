#ifndef _RTP_FILE_

#include "rtputil.h"

/* Opens a MPEG file in read binary mode */
FILE* openMPEG(char*);

/* Creates buffered memory and copyies from file to buffer */
void loadFiletoMem(FILE*, long int, int, mem_t*);
#define _RTP_FILE_
#endif
